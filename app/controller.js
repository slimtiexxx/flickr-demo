/**
 * Created by andras on 17/08/17.
 */
;(function() {

    angular
        .module('flickr')
        .controller('MainController', ['$scope', '$location', 'Flickr', function ($scope, $location, Flickr) {
            $scope.filters = {
                "Animals": {
                    "Pets": {
                        "Guppy": {},
                        "Parrot": {},
                        "GoldFish": {},
                        "Dog": {},
                        "Cat": {}
                    },
                    "Wild": {
                        "Tiger": {},
                        "Ant": {},
                        "Tetra": {},
                        "Peafowl": {},
                        "Mongoose": {}
                    },
                    "Domestic": {
                        "Cow": {},
                        "Pig": {},
                        "Goat": {},
                        "Horse": {}
                    }
                },
                "Food": {
                    "Fast": {
                        "Cheeseburger": {},
                        "Hamburger": {}
                    },
                    "Dessert": {
                        "Chocolate": {},
                        "Cookie": {},
                        "Cake": {},
                        "Pie": {}
                    }
                },
                "Vechicle": {
                    "Motorcycle": {
                        "Harley": "2017-08-21T18:20:49.201Z"
                    },
                    "Car": {
                        "Lamborghini": {},
                        "Ferrari": {},
                        "Bugatti": {},
                        "BMW": {},
                        "Mercedes": {}
                    }
                },
                "Movie": {
                    "Science": {
                        "Sunshine": {},
                        "Interstellar": {},
                        "The": {},
                        "Oblivion": {},
                        "Star": {}
                    }
                }
            };

            $scope.text = 'Goat';
            $scope.loadedImages = 0;
            $scope.imagesPerPage = 50;
            $scope.filterElems = [];


            $scope.filterRemove = function (filter) {
                $scope.loading = true;

                newFilterElems = $scope.filterElems.filter(function(el) {
                    return el !== filter;
                });

                $scope.filterElems = newFilterElems;

                $scope.search($scope.text, $scope.filterElems.toString(), $scope.imagesPerPage);
            };

            $scope.filterUpdate = function (value, event) {
                $scope.loading = true;

                if (event.target.checked) {

                    $scope.filterElems.push(value);
                    $scope.search($scope.text, $scope.filterElems.toString(), $scope.imagesPerPage);

                } else {

                    newFilterElems = $scope.filterElems.filter(function(el) {
                        return el !== value;
                    });

                    $scope.filterElems = newFilterElems;

                    $scope.search($scope.text, $scope.filterElems.toString(), $scope.imagesPerPage);
                }
            };

            $scope.search = function(search, tags, imagesPerPage){
                $scope.loading = true;

                var promise = Flickr.search(search, tags, imagesPerPage);

                promise.then(function(data) {

                    $scope.totalFound = data.photos.total;


                    if (Number($scope.totalFound) == 0) {
                        $scope.noResults = true;
                        $scope.loading = false;
                    } else {
                        $scope.noResults = false;
                        $scope.photos = data.photos.photo;
                    }

                }, function(reason) {
                    console.log('Failed: ' + reason);
                    $scope.loading = false;
                });
            };
        }]);

})();