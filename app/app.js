/**
 * Created by andras on 17/08/17.
 */
;(function() {

    angular
        .module('flickr', [
            'ngRoute',
            'flickrServices'
        ])
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];

    function config($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(false);

        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
            })
            .otherwise({
                redirectTo: '/',
            });

    }

})();
