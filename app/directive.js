/**
 * Created by andras on 27/08/17.
 */

angular.module('flickr')
    .directive('imageonload', function() {

        var loadedImages = 0;

        return {
            restrict: 'A',
            link: function(scope, element, attrs) {

                element.bind('load', function() {
                    loadedImages += 1;

                    var sizes = {
                        'Width' : element[0].naturalWidth,
                        'Height' : element[0].naturalHeight,
                    };

                    element.parent().css({'width': element.context.naturalWidth, 'height' : element.context.naturalHeight})

                    scope.$apply(function(){
                        scope.$parent.loadedImages = loadedImages;
                        scope.$parent.photos[loadedImages - 1].sizes = sizes;


                        if (scope.$parent.imagesPerPage >= Number(scope.$parent.totalFound)) {
                            scope.$parent.imagesPerPage = Number(scope.$parent.totalFound);
                        }

                        if (loadedImages == scope.$parent.imagesPerPage) {
                            var wall = new Freewall(".flickr-photos");
                            wall.reset({
                                selector: '.flickr-photo',
                                animate: true,
                                cellW: 300,
                                cellH: 150,
                                onResize: function() {
                                    wall.fitWidth();
                                },
                            });
                            wall.fitWidth();
                            scope.$parent.loading = false;
                            loadedImages = 0;
                        }
                    });
                });
            }
        };
    });

angular.module('flickr')
    .directive('toggleClass', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                    element.siblings('ul').toggleClass(attrs.toggleClass);
                });
            }
        };
    });

angular.module('flickr')
    .directive('unCheck', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function() {

                    var anchor = element.siblings('span').text().toString();
                    $('p:contains("'+ anchor +'")').siblings('input').attr('checked', false)

                });
            }
        };
    });

angular.module('flickr')
    .directive('scrollOnClick', function() {
        return {
            restrict: 'EA',
            template:'<a title="Scroll Top" class="scrollup icomoon-circle-up"></a>',
            link: function(scope, $elm) {

                $(window).scroll(function () {
                    if ($(this).scrollTop() > 300) {
                        $('.scrollup').addClass('visible');
                    } else {
                        $('.scrollup').removeClass('visible');
                    }
                });

                $elm.on('click', function() {
                    $("html,body").animate({scrollTop: '0px'}, "slow");
                });
            }
        }
    });