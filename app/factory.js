/**
 * Created by andras on 17/08/17.
 */
;(function() {

    angular
        .module('flickrServices',['ngResource'])
        .factory('Flickr',['$http', '$q', function($http, $q) {
            var self = this;
            self.api_key = "4e893b66db9e0ecefcea6688938b7b16";
            self.base_url = "https://api.flickr.com/services/rest/";

            self.search = function(search, tags, imagesPerPage){
                deferred = $q.defer();

                params = {
                    api_key: self.api_key,
                    format: 'json',
                    nojsoncallback: 1,
                    per_page: imagesPerPage,
                    page: 1,
                    tag_mode: 'all',
                    method: (search.length + tags.length != 0 && search.length + tags.length != 0) ? 'flickr.photos.search' : 'flickr.photos.getRecent'
                };

                if ((search != null && search.length > 0)) {
                    params.text = search;
                }

                if ((tags != null && tags.length > 0)) {
                    params.tags = tags;
                }

                $http({method: 'GET', url: self.base_url, params: params})
                    .then(function(data, status, headers, config) {
                        deferred.resolve(data.data);
                    }, function(reason) {
                        deferred.reject(status);
                    });

                return deferred.promise;

            };

            return this;
        }]);



})();