# Flickr Image Searcher

## Introduction

> This is a realtime JS image searcher using Flickr API jQuery, and AngularJS.

## View on the web
http://andrewsite.webuda.com/flickr-demo/


## Installation

1. Clone the project `git clone https://slimtiexxx@bitbucket.org/slimtiexxx/flickr-demo.git`.
2. Go to project folder `cd flickr-demo` and install dependencies via `npm install`.
3. Run `gulp` to run the project locally or `gulp build` to build the project.
